﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Rushkar">
//     Copyright Rushkar. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace YudooApp.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        #region ShopOwner
        public const string ShopOwner_Upsert = "ShopOwner_Upsert";
        public const string ShopOwner_All = "ShopOwner_All";
        public const string ShopOwner_ById = "ShopOwner_ById";
        public const string ShopOwner_ActInAct = "ShopOwner_ActInAct";
        public const string ShopOwner_Delete = "ShopOwner_Delete";
        #endregion

    }
}
