﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YudooApp.Common;
using YudooApp.Common.Paging;
using YudooApp.Data.Contract;
using YudooApp.Entities.Contract;
using YudooApp.Entities.V1;
using Dapper;

namespace YudooApp.Data.V1
{
    public class ShopOwnerDao : AbstractShopOwnerDao
    {


        public override SuccessResult<AbstractShopOwner> ShopOwner_Upsert(AbstractShopOwner abstractShopOwner)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShopOwner.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShopName", abstractShopOwner.ShopName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ShopOwnerName", abstractShopOwner.ShopOwnerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CategoryId", abstractShopOwner.CategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@AddressLine1", abstractShopOwner.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine2", abstractShopOwner.AddressLine2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LandMark", abstractShopOwner.LandMark, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PinCode", abstractShopOwner.PinCode, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Ratings", abstractShopOwner.Ratings, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RatingsBy", abstractShopOwner.RatingsBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@GstNumber", abstractShopOwner.GstNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MobileNumber", abstractShopOwner.MobileNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PersonalPanNumber", abstractShopOwner.PersonalPanNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyPanNumber", abstractShopOwner.CompanyPanNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyRegisterAddress", abstractShopOwner.CompanyRegisterAddress, dbType: DbType.String, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_Upsert, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }


        public override PagedList<AbstractShopOwner> ShopOwner_All(PageParam pageParam, string search)
        {
            PagedList<AbstractShopOwner> ShopOwner = new PagedList<AbstractShopOwner>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_All, param, commandType: CommandType.StoredProcedure);
                ShopOwner.Values.AddRange(task.Read<ShopOwner>());
                ShopOwner.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShopOwner;
        }


        public override SuccessResult<AbstractShopOwner> ShopOwner_ById(long Id)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_ById, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }


        public override SuccessResult<AbstractShopOwner> ShopOwner_ActInAct(long Id)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_ActInAct, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_Delete(long Id , long DeletedBy)
        {
            SuccessResult<AbstractShopOwner> ShopOwner = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", DeletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShopOwner_ById, param, commandType: CommandType.StoredProcedure);
                ShopOwner = task.Read<SuccessResult<AbstractShopOwner>>().SingleOrDefault();
                ShopOwner.Item = task.Read<ShopOwner>().SingleOrDefault();
            }

            return ShopOwner;
        }

    }
}
