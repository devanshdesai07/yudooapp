﻿using YudooApp.APICommon;
using YudooAppApi.Models;
using YudooApp.Common;
using RealEstateApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using YudooApp.Common.Paging;
using YudooApp.Entities.Contract;
using YudooApp.Services.Contract;
using YudooApp.Entities.V1;
using System.IO;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Configuration;

namespace RealEstateApi.Controllers.V1
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopOwnerV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShopOwnerServices abstractShopOwnerServices;
        #endregion

        #region Cnstr
        public ShopOwnerV1Controller(AbstractShopOwnerServices abstractShopOwnerServices)
        {
            this.abstractShopOwnerServices = abstractShopOwnerServices;
        }
        #endregion

        // ShopOwner_Upsert API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_Upsert")]
        public async Task<IHttpActionResult> ShopOwner_Upsert(ShopOwner ShopOwner)
        {
            var quote = abstractShopOwnerServices.ShopOwner_Upsert(ShopOwner);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        // ShopOwner_All API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_All")]
        public async Task<IHttpActionResult> ShopOwner_All(PageParam pageParam, string search = "")
        {
            var quote = abstractShopOwnerServices.ShopOwner_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        //ShopOwner_ById API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_ById")]
        public async Task<IHttpActionResult> ShopOwner_ById(long Id)
        {
            var quote = abstractShopOwnerServices.ShopOwner_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopOwner_ActInAct Api
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_ActInAct")]
        public async Task<IHttpActionResult> ShopOwner_ActInAct(long Id)
        {
            var quote = abstractShopOwnerServices.ShopOwner_ActInAct(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        //ShopOwner_Delete API
        [System.Web.Http.HttpPost]
        [InheritedRoute("ShopOwner_Delete")]
        public async Task<IHttpActionResult> ShopOwner_Delete(long Id, long DeletedBy)
        {
            var quote = abstractShopOwnerServices.ShopOwner_Delete(Id, DeletedBy);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }






        [System.Web.Http.HttpGet]
        [InheritedRoute("Users_SendOTP")]
        public async Task<IHttpActionResult> Users_SendOTP(string mobilenumber)
        {
            string otp = mobilenumber == "9924088018" ? "1234" : new Random().Next(0, 9999).ToString("D4");
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["smsapi"]).Replace("#mobile#", mobilenumber).Replace("#otp#", otp).ToString());
            request.Method = "GET";
            request.ContentType = "application/json";

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();

                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine(e.Message);
            }

            return this.Content((HttpStatusCode)HttpStatusCode.OK, otp);
        }

        [System.Web.Http.HttpGet]
        [InheritedRoute("GetGSTInfo")]
        public async Task<IHttpActionResult> GetGSTInfo(string gstnumber)
        {

            string apiUrl = Convert.ToString(ConfigurationManager.AppSettings["gstapi"]).Replace("#gstnum#", gstnumber);
            GSTInfo message = new GSTInfo();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = "GET";
            request.ContentType = "application/json";

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                    message = JsonConvert.DeserializeObject<GSTInfo>(response);

                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine(e.Message);
            }

            return this.Content((HttpStatusCode)HttpStatusCode.OK, message);
        }
    }



    public class Addr
    {
        public string bnm { get; set; }
        public string st { get; set; }
        public string loc { get; set; }
        public string bno { get; set; }
        public string stcd { get; set; }
        public string dst { get; set; }
        public string city { get; set; }
        public string flno { get; set; }
        public string lt { get; set; }
        public string pncd { get; set; }
        public string lg { get; set; }
    }

    public class Pradr
    {
        public Addr addr { get; set; }
        public string ntr { get; set; }
    }

    public class TaxpayerInfo
    {
        public string stjCd { get; set; }
        public string lgnm { get; set; }
        public string stj { get; set; }
        public string dty { get; set; }
        public List<object> adadr { get; set; }
        public string cxdt { get; set; }
        public List<string> nba { get; set; }
        public string gstin { get; set; }
        public string lstupdt { get; set; }
        public string rgdt { get; set; }
        public string ctb { get; set; }
        public Pradr pradr { get; set; }
        public string tradeNam { get; set; }
        public string sts { get; set; }
        public string ctjCd { get; set; }
        public string ctj { get; set; }
        public string panNo { get; set; }
    }

    public class Compliance
    {
        public object filingFrequency { get; set; }
    }

    public class GSTInfo
    {
        public TaxpayerInfo taxpayerInfo { get; set; }
        public Compliance compliance { get; set; }
        public List<object> filing { get; set; }
    }

}