﻿using YudooApp.Common;
using YudooApp.Common.Paging;
using YudooApp.Data.V1;
using YudooApp.Data.Contract;
using YudooApp.Entities.Contract;
using YudooApp.Services.Contract;

namespace YudooApp.Services.V1
{
    public class ShopOwnerServices : AbstractShopOwnerServices
    {
        private AbstractShopOwnerDao abstractShopOwnerDao;

        public ShopOwnerServices(AbstractShopOwnerDao abstractShopOwnerDao)
        {
            this.abstractShopOwnerDao = abstractShopOwnerDao;
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_Upsert(AbstractShopOwner abstractShopOwner)
        {
            return this.abstractShopOwnerDao.ShopOwner_Upsert(abstractShopOwner); ;
        }

        public override PagedList<AbstractShopOwner> ShopOwner_All(PageParam pageParam, string search)
        {
            return this.abstractShopOwnerDao.ShopOwner_All(pageParam, search);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_ById(long Id)
        {
            return this.abstractShopOwnerDao.ShopOwner_ById(Id);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_ActInAct(long Id)
        {
            return this.abstractShopOwnerDao.ShopOwner_ActInAct(Id);
        }

        public override SuccessResult<AbstractShopOwner> ShopOwner_Delete(long Id, long DeletedBy)
        {
            return this.abstractShopOwnerDao.ShopOwner_Delete(Id, DeletedBy);
        }


    }

}