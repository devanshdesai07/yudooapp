﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YudooApp.Common;
using YudooApp.Common.Paging;
using YudooApp.Entities.Contract;

namespace YudooApp.Services.Contract
{
    public abstract class AbstractShopOwnerServices
    {
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_Upsert(AbstractShopOwner abstractShopOwner);
        public abstract PagedList<AbstractShopOwner> ShopOwner_All(PageParam pageParam, string search);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_ById(long Id);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_ActInAct(long Id);
        public abstract SuccessResult<AbstractShopOwner> ShopOwner_Delete(long Id, long DeletedBy);
    }
}
