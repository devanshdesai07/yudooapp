﻿using YudooApp;
using YudooApp.Entities.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace YudooApp.Entities.Contract
{
    public abstract class AbstractShopOwner
    {
        public long Id { get; set; }
        public string ShopName { get; set; }
        public string ShopOwnerName { get; set; }
        public long CategoryId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string LandMark { get; set; }
        public long PinCode { get; set; }
        public DateTime VideoCall { get; set; }
        public DateTime RegisteredDate { get; set; }
        public bool ShopStatus { get; set; }
        public long Ratings { get; set; }
        public long RatingsBy { get; set; }
        public DateTime RatingsCreatedDate { get; set; }
        public DateTime RatingsUpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public string GstNumber { get; set; }
        public string MobileNumber { get; set; }
        public string PersonalPanNumber { get; set; }
        public string CompanyPanNumber { get; set; }
        public string CompanyRegisterAddress { get; set; }
        public bool IsRegister { get; set; }



        //[NotMapped]
        //public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        //[NotMapped]
        //public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}

