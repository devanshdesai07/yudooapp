﻿using System.Collections.Generic;

namespace YudooApp.Common.Paging
{
    /// <summary>
    /// Paged list interface
    /// </summary>
    public interface IPagedList<T> : IList<T>
    {
        
    }
}
